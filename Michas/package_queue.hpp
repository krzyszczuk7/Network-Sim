/*
 * package_queue.hpp
 *
 *  Created on: Dec 19, 2017
 *      Author: michal
 */

#ifndef PACKAGE_QUEUE_HPP_
#define PACKAGE_QUEUE_HPP_
#include "types.hpp"
#include "IPackageQueue.hpp"
#include <deque>
#include <vector>
#include <functional>
class Package_Queue: public IPackageQueue{
private:
	QueueType _queueType;
	std::deque<Package> _deque;
	std::function<Package()> _popFunction;
public:
	Package_Queue(QueueType _queuetype);
	~Package_Queue();
	void push(const Package& _package);
	Package pop();
	std::vector<Package> viewAll()const;
	QueueType getQueueType()const;
	bool isEmpty()const;
	int size()const;
};

#endif /* PACKAGE_QUEUE_HPP_ */
