#ifndef types_hpp
#define types_hpp

typedef int Time;
typedef int TimeOffset;
typedef int ElementID;
class Package{
private:
  ElementID id;
public:
  static int counter;
  Package(){};
  Package(ElementID _elementID){counter += 1;id=_elementID;};
  ~Package(){counter -=1;};
  ElementID getID(){return id;};

};
enum class QueueType{LIFO,FIFO};
enum class ReceiverType{WORKER,STOREHAUSE};
#endif
