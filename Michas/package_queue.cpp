/*
 * package_queue.cpp
 *
 *  Created on: Jan 9, 2018
 *      Author: michal
 */
#include "package_queue.hpp"
#include "types.hpp"
#include "IPackageQueue.hpp"

std::vector<Package> Package_Queue::viewAll() const {
	std::vector<Package> v(_deque.begin(), _deque.end());
	return v;
}
Package_Queue::Package_Queue(QueueType _queuetype) {
	_queueType = _queuetype;
}

Package_Queue::~Package_Queue() {
}

int Package_Queue::size() const {
	return _deque.size();
}

bool Package_Queue::isEmpty() const {
	return _deque.empty();
}

QueueType Package_Queue::getQueueType() const {
	return _queueType;
}
void Package_Queue::push(const Package& _package) {
	_deque.push_back(_package);
}
Package Package_Queue::pop() {
	Package packageToreturn;
	if (_queueType == QueueType::FIFO) {
		packageToreturn = _deque.back();
		_deque.pop_back();
	} else {
		packageToreturn = _deque.front();
		_deque.pop_front();
	}
	return packageToreturn;
}
