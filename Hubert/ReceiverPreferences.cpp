#include "ReceiverPreferences.h"

void ReceiverPreferences::convertProbWhenAddReceiver(double probManipulatedElem)
{
    double howManyFromEachReceiverProb = probManipulatedElem/receiverProbability.size();
    std::map<std::shared_ptr<IPackageReceiver>, double>::iterator it = receiverProbability.begin();

    std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [&](std::pair<std::shared_ptr<IPackageReceiver>, double> elem){
                      it->second -= howManyFromEachReceiverProb;
                  });
}

void ReceiverPreferences::convertProbWhenDeleteReceiver(double probManipulatedElem)
{
    double howManyFromEachReceiverProb = probManipulatedElem/receiverProbability.size();
    std::map<std::shared_ptr<IPackageReceiver>, double>::iterator it = receiverProbability.begin();

    std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [&](std::pair<std::shared_ptr<IPackageReceiver>, double> elem){
                      it->second += howManyFromEachReceiverProb;
                  });
}

std::map<std::shared_ptr<IPackageReceiver>, double> ReceiverPreferences::getProbabilities()
{
    return receiverProbability;
}

void ReceiverPreferences::addReceiver(const IPackageReceiver *addedReceiver)
{
    double probability = 0.1;
    convertProbWhenAddReceiver(probability);
    receiverProbability.insert(std::make_pair(std::make_shared<IPackageReceiver>(*addedReceiver), probability));
}


void ReceiverPreferences::addReceiver(IPackageReceiver *addedReceiver, double probability)
{
    convertProbWhenAddReceiver(probability);
    receiverProbability.insert(std::make_pair(std::make_shared<IPackageReceiver>(*addedReceiver), probability));
}


void ReceiverPreferences::setProbabilities(const IPackageReceiver *wantedReceiver, double probability)
{
    double oldProbabilityOfSetReceiver;

    std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [=](std::pair<std::shared_ptr<IPackageReceiver>, double> elem){
                      if(elem.first.get() == wantedReceiver)
                          elem.second = oldProbabilityOfSetReceiver;
                  });

    double howManyFromEachReceiverProb = oldProbabilityOfSetReceiver/(receiverProbability.size() - 1);

    std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [=](std::pair<std::shared_ptr<IPackageReceiver>, double> elem){
                      if(elem.first.get() != wantedReceiver)
                          if(oldProbabilityOfSetReceiver > probability)
                              elem.second -= howManyFromEachReceiverProb;
                          else
                              elem.second += howManyFromEachReceiverProb;
                  });

    std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [=](std::pair<std::shared_ptr<IPackageReceiver>, double> elem){
                      if(elem.first.get() == wantedReceiver)
                      elem.second = probability;
                  });
}


void ReceiverPreferences::removeReceiver(IPackageReceiver *toDeleteReceiver)
{
    double probabilityOfRemovedReceiver;
    std::map<std::shared_ptr<IPackageReceiver>, double>::iterator it;

    /*std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [=](std::pair<std::shared_ptr<IPackageReceiver>, double> elem)mutable{
                      if(elem.first.get() == toDeleteReceiver) {
                          elem.second = probabilityOfRemovedReceiver;
                      }
                  });
    */

    for(it = receiverProbability.begin(); it != receiverProbability.end(); ++it)
    {
        if(it->first.get() == toDeleteReceiver) {
            it->second = probabilityOfRemovedReceiver;
            receiverProbability.erase(it);
        }
    }

    convertProbWhenDeleteReceiver(probabilityOfRemovedReceiver);

     }

IPackageReceiver* ReceiverPreferences::draw()
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(0.0, 1.0);
    double randome = dist(mt);

    std::map<std::shared_ptr<IPackageReceiver>, double>::iterator close;

    std::vector<double> probabilitiesFromMap;
    //std::map<std::shared_ptr<IPackageReceiver>, double>::iterator it = receiverProbability.begin();

    /*std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [this](std::vector<double> probabilitiesFromMap){
                      probabilitiesFromMap.push_back(receiverProbability);
                  })
    */
    for(std::map<std::shared_ptr<IPackageReceiver>, double>::iterator it = receiverProbability.begin(); it != receiverProbability.end(); it++)
    {
        probabilitiesFromMap.push_back(it->second);
    }

    std::sort(probabilitiesFromMap.begin(), probabilitiesFromMap.end());
    //std::vector<double>::iterator lookingForTheNearestProb;
    auto lookingForTheNearestProb = std::lower_bound (probabilitiesFromMap.begin(), probabilitiesFromMap.end(), randome);
    double look = *lookingForTheNearestProb;

    while(close != receiverProbability.end())
    {
        if(close->second == look)
            return close->first.get();
    }



}

double ReceiverPreferences::getProb(const IPackageReceiver* rec)
{
    std::map<std::shared_ptr<IPackageReceiver>, double>::iterator it;

    /*std::for_each(receiverProbability.begin(), receiverProbability.end(),
                  [=](std::pair<std::shared_ptr<IPackageReceiver>, double> elem)mutable{
                      if(elem.first.get() == toDeleteReceiver) {
                          elem.second = probabilityOfRemovedReceiver;
                      }
                  });
    */

    for(it = receiverProbability.begin(); it != receiverProbability.end(); it++)
    {
        double prob;
        if(it->first.get() == rec) {
            return it->second ;
        }
    }
}
