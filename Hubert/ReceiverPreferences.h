#ifndef RECEIVERPREFERENCES_H
#define RECEIVERPREFERENCES_H

#include<map>
#include<algorithm>
#include<memory>
#include<iterator>
#include<chrono>
#include<random>

class IPackageReceiver{};

class ReceiverPreferences
{
private:
    std::map<std::shared_ptr<IPackageReceiver>, double> receiverProbability;

    void convertProbWhenAddReceiver(double probManipulatedElem);
    /* helper method -> count new probability for each receiver in meanwhile of adding
     * extra receiver; calculate first how much from each probability should substract to
     * still have place at proper probability*/

    void convertProbWhenDeleteReceiver(double probManipulatedElem);


public:
    std::map<std::shared_ptr<IPackageReceiver>, double> getProbabilities();//done
    void setProbabilities(const IPackageReceiver* wantedReceiver, double probability);//done
    void addReceiver(const IPackageReceiver* addedReceiver);//done
    void addReceiver(IPackageReceiver *addedReceiver, double probability);//done
    void removeReceiver(IPackageReceiver *toDeleteReceiver);//done
    //std::pair<std::shared_ptr<IPackageReceiver>, double>  view();
    IPackageReceiver* draw();//losuje zwraca konkretny wskązni na obiet po wylosowaniu
    double getProb(const IPackageReceiver* rec);

};

#endif //RECEIVERPREFERENCES_H
