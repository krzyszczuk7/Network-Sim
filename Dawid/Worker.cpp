#include "Worker.hpp"

Worker::Worker(ElementID _id, TimeOffset _pDuration, IPackageQueue* _queue)
{

id = _id;
processingDuration = _pDuration;
queue = _queue;

};

TimeOffset Worker::getProcessingDuration() { return processingDuration; }

Time Worker::getPackageProcessingStartTime() { return packageProcessingStartTime; }

ReceiverType Worker::getReceiverType() { return ReceiverType::WORKER; }

ElementID Worker::getElementID() { return id; }

void Worker::receivePackage(Package _package){

currentlyProcessedPackages.push_back(_package);

}

void Worker::doWork() {

	if(IPackageQueue::getQueueType() == QueueType::FIFO))
		{}

		sendingBuffer.push_back(currentlyProcessedPackages.back());
		currentlyProcessedPackages.erase(currentlyProcessedPackages.end());

	if(IPackageQueue::getQueueType() == QueueType::LIFO)
		{}
	sendingBuffer.push_back(currentlyProcessedPackages.front());
	currentlyProcessedPackages.erase(currentlyProcessedPackages.begin());
}
