#include "PackageSender.hpp"


std::vector<Package> PackageSender::getSendingBuffer()
{return sendingBuffer;}

void PackageSender::sendPackage(){

	auto currentReceiver = IPackageReceiver* ReceiverPreferences::draw();
	currentReceiver -> receivePackage(sendingBuffer.front());
	sendingBuffer.erase(sendingBuffer.begin());

}
