#ifndef WORKER_HPP_
#define WORKER_HPP_
#include "PackageSender.hpp"
#include "interfaces.hpp"
#include "types.hpp"

class Worker : public PackageSender , public IPackageReceiver{

private:
	ElementID id;
	TimeOffset processingDuration;
	Time packageProcessingStartTime;
	IPackageQueue* queue;
	std::vector<Package> currentlyProcessedPackages;

public:
	Worker(ElementID _id, TimeOffset _pDuration, IPackageQueue* _queue);
	void receivePackage(Package _package);
	std::vector<Package> viewQueue();
	void doWork();
	TimeOffset getProcessingDuration();
	Time getPackageProcessingStartTime();
	ReceiverType getReceiverType();
	ElementID getElementID();
};

#endif /* WORKER_HPP_ */
