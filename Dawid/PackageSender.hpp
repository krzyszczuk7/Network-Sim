#ifndef PACKAGE_SENDER_HPP_
#define PACKAGE_SENDER_HPP_
#include "interfaces.hpp"
#include "ReceiverPreferences.hpp"

class PackageSender{

private:
	ReceiverPreferences _receiverPreferences;
	std::vector<Package> sendingBuffer;

public:
	void sendPackage();
	std::vector<Package> getSendingBuffer();

};

#endif /* PACKAGE_SENDER_HPP_ */
