/*
 * IPackageQueue.hpp
 *
 *  Created on: Jan 5, 2018
 *      Author: michal
 */

#ifndef IPACKAGEQUEUE_HPP_
#define IPACKAGEQUEUE_HPP_
#include "types.hpp"
#include <vector>
class IPackageQueue{
public:
	virtual~IPackageQueue(){};
	virtual void push(const Package& _package)=0;
	virtual Package pop()=0;
	virtual std::vector<Package> viewAll()const=0;
	virtual QueueType getQueueType()const=0;
	virtual bool isEmpty()const=0;
	virtual int size()const=0;
};

#endif /* IPACKAGEQUEUE_HPP_ */
